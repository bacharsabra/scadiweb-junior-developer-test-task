<?php
$servername = "localhost";
$username = "root";
$password = "";
$db="scandiweb";

// Create connection
$mysqli = new mysqli($servername, $username, $password, $db);

// Check connection
if ($mysqli->connect_error) {
	echo "ERROR Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	die("Connection failed: " . $mysqli->connect_error);
}

?>