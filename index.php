<?php include 'connection.php';
session_start();

if(isset($_POST['delete-btn']))
{
    $all_SKU = $_POST['checkbox'];
    $extract_SKU = implode(',' , $all_SKU);
    echo $extract_SKU;

    $query = "DELETE FROM products WHERE SKU IN($extract_SKU)";
    $query_run = mysqli_query($mysqli, $query);

    if($query_run)
    {
        header("Location: index.php");
    }
    else{
        echo "error detected";
        header("Location: index.php");
    }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Scandishop</title>
    <!--Bootstrap-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--CSS-->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/reset.css" rel="stylesheet">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Exo+2:100,200,300,400,400i,500,600,700" rel="stylesheet">

    <style>
      body {
        font-family: 'Exo 2';
      }

      .container {
        padding-right: 20px;
        padding-left: 20px;
      }

      .navbar-default{
        padding-bottom: 15px;
      }

      .main-footer {
        padding: 8px;
        position: absolute;
        background-color: #14171A;
        left: 0;
        bottom: 0;
        width: 100%;
        text-align: center;
      }

      #page-container {
        position: relative;
        min-height: 51vh;
      }

    </style>
  </head>

  <body>
    <div class="banner">
      <div class="container">
        <div>
            <h1>Shop</h1>
        </div>
      </div>
    </div>
    <form method="POST">
      <div class="add-delete">
          <button type="submit" id="delete-product-btn" name="delete-btn" style="float: right; margin-right: 10%; margin-top:2%">MASS DELETE</button>
          <button type="submit" id="add-product-btn" style="float: right; margin-right: 2%; margin-top:2%"><a href="add-product.php">ADD</a></button>
      </div>
      <!-- Product Section -->
      <section id="product">
        <div class="container">
          <h2 style="padding-top: 40px;">Latest Products</h2>
            <div class="row list"  id="result" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine" data-aos-duration="500">
              <?php
                require_once('connection.php');
                $sql="SELECT * FROM products";

                $RESULT=$mysqli->query($sql);
                if(!$sql) {
                  echo "Error: Our query failed to execute and here is why: \n";
                  echo "Query: " . $sql . "\n";
                  echo "Errno: " . $mysqli->errno . "\n";
                  echo "Error: " . $mysqli->error . "\n";
                  die();
                }

                while($row=$RESULT->fetch_assoc()){

              ?>
              <div class="col-sm-6 col-md-3 col">
                <div class="thumbnail" style="text-align:center;">
                  <div class="caption">
                    <h1><?=$row['SKU'] ?></h1>
                    <h3><?=$row['Name'] ?></h3>
                    <p style="word-wrap: break-word;"> <?=$row['Type']?></p>
                    <p><?=$row['Attribute']?></p>
                    <div class="box">
                      <p><span><?=$row['Price'] ?> $</span> Only</p>
                      <input type="checkbox" style="position:absolute; right:38px; bottom: 45px;" class="delete-checkbox" name="checkbox[]" value="<?=$row['SKU'];?>">
                    </div>
                  </div>
                </div>
              </div>
              <?php
                }
                $mysqli->close();

              ?>
            </div>
        </div>
      </section>
    </form>
    <!-- Footer Section -->
    <div id="page-container">
      <footer class="main-footer">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <p class="footer-logo">Scandi<span>shop</span></p>
            </div>
            <div class="col-sm-5">
              <span><a href="https://www.notion.so/Junior-Developer-Test-Task-1b2184e40dea47df840b7c0cc638e61e">Scandiweb Test Assignment</a></span>
            </div>
            <div class="col-sm-9">
              <p>(C) 2021, All Rights Reserved <span><a href="https://www.template.net/editable/websites/html5">Petz Theme</a></span>, Designed by Bachar Sabra</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- JavaScript-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
      <script src="js/animate.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/custom.js"></script>
  </body>
</html>