<?php
  session_start();
?>

<?php

	function addProduct() {
		require_once('connection.php');
		$SKU = $mysqli->real_escape_string($_POST['addsku']);
		$Name = $mysqli->real_escape_string($_POST['addname']);
		$Price = $mysqli->real_escape_string($_POST['addprice']);
		$Type = $_POST['productType'];

		switch ($Type) {
			case 'DVD':
				$Size = $_POST['addsize'];

        $data = $Size.' MB';
				$Attribute = $mysqli->real_escape_string($data);
			break;
			case 'Book':
				$Weight = $_POST['addweight'];

        $measurment = $Weight.' KG';
				$Attribute = $mysqli->real_escape_string($measurment);
			break;
			case 'Furniture':
				$Height = $_POST['addheight'];
				$Width = $_POST['addwidth'];
				$Length = $_POST['addlength'];

				$dimensions = $Height.'x'.$Width.'x'.$Length. ' CM';
				$Attribute = $mysqli->real_escape_string($dimensions);
			break;
		}

		$sql = $mysqli->query("INSERT INTO products(SKU, Name, Price, Type, Attribute) VALUES ('{$SKU}', '{$Name}', '{$Price}', '{$Type}', '{$Attribute}')");
		if(!$sql) {
			echo "Error: Our query failed to execute and here is why: \n";
			echo "Query: " . $sql . "\n";
			echo "Errno: " . $mysqli->errno . "\n";
			echo "Error: " . $mysqli->error . "\n";
			die();
		}

		$mysqli->close();
	}

	if(isset($_POST['submit'])) {
		addproduct();
	}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Add Product</title>
    <!--Bootstrap-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--CSS-->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Exo+2:100,200,300,400,400i,500,600,700" rel="stylesheet">

    <style>
      body {
        font-family: 'Exo 2';

      }

      .container {
        padding-right: 20px;
        padding-left: 20px;
      }

      .navbar-default{
        position: absolute;
        padding-bottom: 15px;
      }

      .main-footer {
        padding: 0px;
        position: absolute;
        background-color: #14171A;
        left: 0;
        bottom: 0;
        width: 100%;
        text-align: center;
      }

      label{
        display: inline-block;
        padding: 3px 6px;
        text-align: left;
        width: 150px;
      }

      select{
        cursor: pointer;
      }

      #page-container {
        position: relative;
        min-height: 83vh;
      }
    </style>
  </head>

  <body>
    <form id="product_form" style="margin-top: 100px; margin-left: 50px" method="POST">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <p class="navbar-brand">Add<span> Products</span></p>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <button style="float: right; margin-right: 10%; margin-top:2%"><a href="index.php">CANCEL</a></button>
              <input type="submit" name="submit" value="SAVE" style="float: right; margin-right: 2%; margin-top:2%"></input>
          </div>
        </div>
      </nav>
      <!-- Add Product Section -->
      <div id="SKU">
        <p>
          <label>SKU:</label>
          <input type="text" id="sku" name="addsku" required oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
        </p>
      </div>

      <div id="Name">
        <p>
          <label>Name:</label>
          <input type="text" id="name" name="addname" required oninvalid="this.setCustomValidity('Please, submit required data.')" oninput="this.setCustomValidity('')"></input>
        </p>
      </div>

      <div id="Price">
        <p>
          <label>Price ($):</label>
          <input type="text" id="price" name="addprice" required oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
        </p>
      </div>

      <p>
        <label>Type Switcher</label>
        <select name="productType" id="productType" required oninvalid="this.setCustomValidity('Please, submit required data.')" oninput="this.setCustomValidity('')">
          <option value="DVD">DVD</option>
          <option value="Book">Book</option>
          <option value="Furniture">Furniture</option>
        </select>
      </p>

      <section class="DVD" id="DVD">
          <div class="size" id="size">
              <p>
                <label>Size (MB):</label>
                <input type="text" id="size" name="addsize" oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
                <br>
                <br>
                <i>Please provide size in megabytes.</i>
              </p>
          </div>
      </section>

      <section class="Book" id="Book">
          <div class="weight" id="weight">
              <p>
                <label>Weight (KG):</label>
                <input type="text" id="weight" name="addweight" oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
                <br>
                <br>
                <i>Please provide weight in kilograms.</i>
              </p>
          </div>
      </section>

      <section class="Furniture" id="Furniture">
          <div class="dimension" id="dimension">
              <p>
                <label>Height (CM):</label>
                <input type="text" id="height" name="addheight" oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
              </p>
              <p>
                <label>Width (CM):</label>
                <input type="text" id="width" name="addwidth" oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
              </p>
              <p>
                <label>Length (CM):</label>
                <input type="text" id="length" name="addlength" oninvalid="checkInput(this);" oninput="checkInput(this);"></input>
                <br>
                <br>
                <i>Please provide dimensions in HxWxL format.</i>
              </p>
          </div>
      </section>
    </form>
    <!-- Footer Section -->
    <div id="page-container">
      <footer class="main-footer">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <p class="footer-logo">Scandi<span>shop</span></p>
            </div>
            <div class="col-sm-5">
              <span><a href="https://www.notion.so/Junior-Developer-Test-Task-1b2184e40dea47df840b7c0cc638e61e">Scandiweb Test Assignment</a></span>
            </div>
            <div class="col-sm-9">
              <p>(C) 2021, All Rights Reserved <span><a href="https://www.template.net/editable/websites/html5">Petz Theme</a></span>, Designed by Bachar Sabra</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <!-- JavaScript-->
    <script>
      function checkInput(textbox) {
        if (textbox.value == '') {
          textbox.setCustomValidity('Please, submit required data.');
        }
        else if(isNaN(textbox.value)){
          textbox.setCustomValidity('Please, provide the data of indicated type.');
        }
        else {
          textbox.setCustomValidity('');
        }
          return true;
      }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://www.addressfinder.co.nz/assets/v2/widget.js"></script>
    <script src="js/animate.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/form.js"></script>
  </body>
</html>